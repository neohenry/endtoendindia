package com.allstate.rest;

import com.allstate.entities.Employee;
import com.allstate.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeController {

    @Autowired
    private EmployeeService service;


    @RequestMapping(value="/status", method= RequestMethod.GET)
    public String getStatus()
    {
        return "Employee Api is running";
    }

    @RequestMapping(value="/total", method= RequestMethod.GET)
    public long getTotal()
    {
        return service.total();
    }

    @RequestMapping(value="/all", method= RequestMethod.GET)
    public List<Employee> getAll()
    {
        return service.all();
    }

    @RequestMapping(value="/find/{id}", method= RequestMethod.GET)
    public Employee find(@PathVariable("id") int id)
    {
        return service.findById(id);
    }

    @RequestMapping(value="/save", method= RequestMethod.POST)
    public void save(@RequestBody Employee employee)
    {
        service.save(employee);
    }

    @RequestMapping(value="/update/{id}", method= RequestMethod.PUT)
    public void update(@RequestBody Employee employee, @PathVariable("id") int id)
    {
        employee.setId(id);
        service.update(employee);
    }
}
