package com.allstate.service;

import com.allstate.dao.EmployeeData;
import com.allstate.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeData dao;

    @Override
    public long total() {
        return dao.count();
    }

    @Override
    public List<Employee> all() {
        return dao.findall();
    }

    @Override
    public void save(Employee employee) {
        dao.save(employee);
    }

    @Override
    public Employee findById(int id) {
        return dao.find(id);
    }

    @Override
    public void update(Employee employee) {
        dao.update(employee);
    }
}
