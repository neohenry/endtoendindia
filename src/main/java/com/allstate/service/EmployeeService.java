package com.allstate.service;

import com.allstate.entities.Employee;

import java.util.List;

public interface EmployeeService {
    long total();

    List<Employee> all();

    void save(Employee employee);

    Employee findById(int id);

    void update(Employee employee);
}
