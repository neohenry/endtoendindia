package com.allstate.di;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Car {
    private String make = "BMW";
    private String model = "3 Series";

    private Engine petrolEngine;

    private Engine dieselEngine;

    public Engine getPetrolEngine() {
        return petrolEngine;
    }

    public Engine getDieselEngine() {
        return dieselEngine;
    }

    @Autowired
    public void setPetrolEngine(PetrolEngine engine) {
        this.petrolEngine = engine;
    }

    @Autowired
    public void setDieselEngine(DieselEngine engine) {
        this.dieselEngine = engine;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
