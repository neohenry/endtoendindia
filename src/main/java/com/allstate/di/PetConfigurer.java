package com.allstate.di;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan("com.allstate")
public class PetConfigurer {
    // @Bean
    // public Owner owner(@Autowired Pet pet1) {
    //     return new Owner(pet1);
    // }

    // @Bean
    // public Pet pet1() {
    //     return new Cat();
    // }

    // @Bean
    // public Pet pet2() {
    //     return new Dog();
    // }
    
}
