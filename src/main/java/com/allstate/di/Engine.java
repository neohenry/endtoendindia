package com.allstate.di;

public interface Engine {
    double getEngineSize();

    void setEngineSize(double engineSize);
}
