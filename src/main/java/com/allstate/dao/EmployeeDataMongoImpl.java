package com.allstate.dao;

import java.util.List;

import com.allstate.entities.Employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeDataMongoImpl implements EmployeeData {

    @Autowired
    MongoTemplate tpl;

    @Override
    public void save(Employee employee) {
        tpl.save(employee);
    }

    @Override
    public void update(Employee employee) {
        Employee updateEmployee = tpl.findOne(
                Query.query(Criteria.where("id").is(employee.getId())), Employee.class);
        updateEmployee.setName(employee.getName());
        updateEmployee.setEmail(employee.getEmail());
        updateEmployee.setSalary(employee.getSalary());
        updateEmployee.setAge(employee.getAge());
        tpl.save(updateEmployee);
    }

    @Override
    public long count() {
        Query query = new Query();
        long result = tpl.count(query, Employee.class);
        return result;
    }

    @Override
    public Employee find(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Employee employee = tpl.findOne(query, Employee.class);
        return employee;
    }


    @Override
    public List<Employee> findall() {
        return tpl.findAll(Employee.class);
    }

}
