package com.allstate.dao;

import java.util.List;

import com.allstate.entities.Employee;

public interface EmployeeData {
    long count();

    Employee find(int id);

    List<Employee> findall();

    void save(Employee employee);

    void update(Employee employee);
}