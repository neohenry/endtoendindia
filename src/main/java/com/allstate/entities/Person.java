package com.allstate.entities;

import org.springframework.data.annotation.Id;

public abstract class Person {
    @Id
    private int id;
    private String name;
    private int age;
    private final static int ageLimit = 100;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    protected void display() {
        System.out.printf("values are %s, %d", this.name, this.id);
    }

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public Person() {
    }

    public static int getAgelimit() {
        return ageLimit;
    }
}
